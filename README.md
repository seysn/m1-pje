PJE5 : Web of Thing
===========================

TP1
----

Cette première séance était globalement destinée à découvrir comment manipuler le mbed et ses outils. Nous l'avons manipulé avec l'editeur en ligne de mbed qui utilise le langage C++.

Nous avons tout d'abord utilisé les differentes led internes en utilisant la classe DigitalOut (de la librairie C++ de mbed).
De manière empirique, nous avons constaté que passer la valeur de l'objet à 1 allume la LED correspondante (dénommés LED1, LED2, ...), 0 permettant de l'éteindre.


Par la suite, nous avons branché le mbed sur la plaquette afin de pouvoir allumer une LED externe. Nous avons commencé par relier la sortie VOUT diffusant 3,3V en continu à une branche de la LED. Puis nous avons relié l'autre branche à la pin GND afin de fermer le circuit.

Pour aller plus loin, nous avons utilisé les sorties PWMOUT avec les classes associées afin de pouvoir gérer l'intensité du courant.

Le code ci-dessous permet au final de faire clignoter la LED de façon à se qu'elle s'allume et s'éteigne lentement.

```cpp
#include "mbed.h"

PwmOut led(p21);

int main() {
    bool up = true;
    while(1) {
        if (up)
            led = led + 0.01;
        else
            led = led - 0.01;
        wait(0.01);
        if(led == 1.0 || led == 0.0) {
            up = !up;
        }
    }
}
```


```cpp
#include "mbed.h"

PwmOut led(p21);
AnalogIn ain(p20);

int main() {
    bool up = true;
    while(1) {
        if (ain > 0.5) {
            if (up)
                led = led + 0.01;
            else
                led = led - 0.01;
            wait(0.01);
            if(led == 1.0 || led == 0.0) {
                up = !up;
            }
        } else led = 0;
    }
}
```
